import React, { Suspense, useEffect } from 'react';

import useManageState from '../utils/useManageState';
import { Container, Alert } from 'react-bootstrap';
import Spinner from '../Spinner';
const TableComponent = React.lazy(() => import('../TableComponent'));
// import TableComponent from '../TableComponent';
const ArticleList = () => {
  const {
    articles,
    query,
    page,
    loading,
    inputValue,
    error,
    setInputValue,
    handleScroll,
    appendArticles,
    getNewArticles,
    handleClick,
  } = useManageState();

  console.log(articles);
  // calling funtion on scroll
  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
  }, []);

  // call funtion on search
  useEffect(() => {
    getNewArticles();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query]);

  // call funtion on load more
  useEffect(() => {
    if (articles.length !== 0) appendArticles();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  return (
    <Container className='mt-5'>
      <form className='d-flex'>
        <input
          className='form-control me-2'
          type='search'
          placeholder='Search'
          aria-label='Search'
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
        />
        <button
          className='btn btn-outline-primary'
          onClick={(e) => handleClick(e)}
        >
          Search
        </button>
      </form>

      <hr />

      <Suspense fallback={<Spinner />}>
        <TableComponent articles={articles} />
      </Suspense>

      {loading && <Spinner />}
      {error && <Alert variant='danger'>{error}</Alert>}
    </Container>
  );
};

export default ArticleList;
