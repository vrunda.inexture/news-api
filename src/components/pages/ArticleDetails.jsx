import React from 'react';
import { Container } from 'react-bootstrap';
import { Navigate, useLocation } from 'react-router-dom';

const ArticleDetails = () => {
  // get article from state
  let location = useLocation();

  const { article } = location.state;
  console.log(article);

  return article ? (
    <Container className='my-3'>
      <h1>{article.title}</h1>

      {article.author && (
        <p className='text-muted fst-italic'>Author: {article.author} </p>
      )}
      <p className='text-muted fst-italic'>
        Published At: {article.publishedAt}
      </p>

      <hr />
      <div className='mb-3'>
        <img
          src={article.urlToImage}
          alt='siteimage'
          className='w-100 img-fluid m-auto'
        />
      </div>
      <div className='py-3'>
        <p>{article.description}</p>
        <p>{article.content}</p>

        <p>
          You can find the original post here:
          <br />
          <a href={article.url}>{article.url} </a>
        </p>
        <p className='text-muted fst-italic ms-auto'>
          Source: {article.source.name}
        </p>
      </div>
    </Container>
  ) : (
    <Navigate to='/' />
  );
};

export default ArticleDetails;
