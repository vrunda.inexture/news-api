import React from 'react';
import { Link } from 'react-router-dom';
import { Table } from 'react-bootstrap';

const TableComponent = ({ articles }) => {
  return (
    <Table className='table table-striped table-hover'>
      <thead>
        <tr>
          <th>#</th>
          <th>Title</th>
          <th>Source</th>
          <th style={{ width: '200px' }}>Published At</th>
        </tr>
      </thead>
      <tbody>
        {articles.map((article, index) => (
          <tr key={index}>
            <th>{index + 1}</th>
            <td>{article.title}</td>
            <td>{article.source.name}</td>

            <td>{article.publishedAt}</td>
            <td style={{ width: '100px' }}>
              <Link to={`/${index}`} state={{ article: article }}>
                Read More{' '}
              </Link>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default TableComponent;
