import { useState } from 'react';
import axios from 'axios';

const useManageState = () => {
  const [articles, setArticles] = useState([]);
  const [query, setQuery] = useState('india');
  const [page, setPage] = useState(1);
  const [inputValue, setInputValue] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  // when reached to end of the page load more
  const handleScroll = () => {
    // waits for the data to load
    if (
      Math.ceil(window.innerHeight + document.documentElement.scrollTop) ===
        document.documentElement.offsetHeight &&
      !loading &&
      !error
    ) {
      setPage((prev) => prev + 1);
    } else {
      return;
    }
  };

  // fetching data (api Call)
  const fetchData = async () => {
    let res;
    setLoading(true);
    try {
      res = await axios.get(
        `https://newsapi.org/v2/everything?q=${query}&apiKey=976ae722ad6c401ca61866adc35ff2ca&page=${page}`
      );
      setLoading(false);
      return res.data.articles;
    } catch (err) {
      console.log(err, err.message, err.response);
      setLoading(false);
      setError(err.response.data.message);
      return [];
    }
  };

  // Load More
  const appendArticles = async () => {
    const data = await fetchData();
    setArticles([...articles, ...data]);
  };

  // get new articles
  const getNewArticles = async () => {
    const data = await fetchData();
    setArticles(data);
  };

  // handle search button
  const handleClick = (e) => {
    e.preventDefault();
    if (!inputValue) {
      return;
    }
    setQuery(inputValue);
    setPage(1);
  };

  return {
    articles,
    setArticles,
    query,
    setQuery,
    page,
    setPage,
    loading,
    setLoading,
    inputValue,
    setInputValue,
    error,
    handleScroll,
    appendArticles,
    getNewArticles,
    fetchData,
    handleClick,
  };
};

export default useManageState;
