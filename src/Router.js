import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import ArticleList from './components/pages/ArticleList';
import ArticleDetails from './components/pages/ArticleDetails';
import Header from './components/Header';

const Router = () => {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path='/' element={<ArticleList />} />
        <Route path='/:id' element={<ArticleDetails />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
